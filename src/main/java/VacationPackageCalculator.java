// Copyright (C) 2020
// All rights reserved

import java.util.InputMismatchException;
import java.util.Scanner;


public class VacationPackageCalculator {

	/**
	 * {@value #BASE_COST} The base cost of a vacation package.
	 */
    static final double BASE_COST = 1000;

	/**
	 * {@value #MAX_PASS} The maximum number of passengers allowed.
	 */
    static final double MAX_PASS = 80;

	/**
	 * {@value #MIN_PASS_TO_DISCOUNT}
	 * The minimum number of passengers required for a discount.
	 */
    static final int MIN_PASS_TO_DISCOUNT = 4;

	/**
	 * {@value #MAX_PASS_TO_DISCOUNT}
	 * The maximum number of passengers eligible for a discount.
	 */
    static final int MAX_PASS_TO_DISCOUNT = 10;

	/**
	 * {@value #MIN_DISCOUNT} The minimum discount percentage.
	 */
    static final double MIN_DISCOUNT = 0.1;

	/**
	 * {@value #MAX_DISCOUNT} The maximum discount percentage.
	 */
    static final double MAX_DISCOUNT = 0.1;

	/**
	 * {@value #PENALTY_PASS}
	 * The number of passengers that trigger a penalty fee.
	 */
    static final int PENALTY_PASS = 7;

	/**
	 * {@value #PENALTY_FEE} The penalty fee amount.
	 */
    static final double PENALTY_FEE = 200;

	/**
	 * {@value #PROMOTION_PASS}
	 * The number of passengers required for a promotion.
	 */
    static final int PROMOTION_PASS = 2;

	/**
	 * {@value #PROMOTION_DURATION} The duration of the promotion (in days).
	 */
    static final double PROMOTION_DURATION = 30;

	/**
	 * {@value #PROMOTION_P} The promotion discount amount.
	 */
    static final double PROMOTION_P = 200;
    enum Destination {
    	/**
         * {@value #PARIS_COST} The destination for Paris.
         */
        PARIS(500),

        /**
         * {@value #NEW_YORK_COST} The destination for New York City.
         */
        NEW_YORK_CITY(600),

        /**
         * {@value #OTHER_COST} Other unspecified destination.
         */
        OTHER(0);

    	/**
        * {@value #cost} The cost associated destination.
        */
        private final double cost;

        /**
         * Creates a new destination with the specified cost.
         *
         * @param c The cost associated with the destination.
         */
        Destination(final double c) {
            this.cost = c;
        }

        public double getCost() {
            return cost;
        }
    }
//CHECKSTYLE:OFF
    public static void main(String[] args) {
//CHECKSTYLE:ON
        int destinationI = -1;
        Destination selectedDestination = null;
        int numTravelers = -1;
        int duration = -1;
        double discount = 0.0;
        double penaltyFee = 0.0;
        double promotionPolicy = 0.0;

        Scanner input = new Scanner(System.in);

        System.out.println("Destination of the vacation:");
        int i = 1;

        for (Destination destination : Destination.values()) {
            System.out.println(i + ". " + destination.name());
            i++;
        }

        while (destinationI < 1 || destinationI > Destination.values().length) {
            try {
                System.out.print("Select the destination: ");
                destinationI = input.nextInt();
                if (destinationI < 1
                	|| destinationI > Destination.values().length) {
                    System.out.println("Invalid destination!!");
                }
            } catch (InputMismatchException e) {
                System.out.println(
                		"Invalid input. Please enter a valid number.");
                input.nextLine();
            }
        }

        selectedDestination = Destination.values()[destinationI - 1];

        while (numTravelers < 1 || numTravelers > MAX_PASS) {
            try {
            	System.out.print("Enter the number of travelers: ");
                numTravelers = input.nextInt();
                if (numTravelers < 1) {
                    System.out.println(
                    		"The number of travelers must be at least 1!!");
                }
                if (numTravelers > MAX_PASS) {
                    System.out.println(
                    		"The group cant't be bigger than "
                        + MAX_PASS + " persons!!");
                }
            } catch (InputMismatchException e) {
                System.out.println(
                		"Invalid input. Please enter a valid number.");
                input.nextLine();
            }
        }

        while (duration < 1) {
            try {
            	System.out.print(
            			"Enter the duration of the vacation in days: ");
                duration = input.nextInt();
                if (duration < 1) {
                    System.out.println(
                    		"The number of days must be at least 1!!");
                }
            } catch (InputMismatchException e) {
                System.out.println(
                		"Invalid input. Please enter a valid number.");
                input.nextLine();
            }
        }
        input.close();

        if (numTravelers > MIN_PASS_TO_DISCOUNT
        		&& numTravelers <= MAX_PASS_TO_DISCOUNT) {
            discount = MIN_DISCOUNT;
        } else if (numTravelers > MAX_PASS_TO_DISCOUNT) {
            discount = MAX_DISCOUNT;
        }

        if (duration < PENALTY_PASS) {
            penaltyFee = PENALTY_FEE;
        }

        if (duration > PROMOTION_DURATION || numTravelers == PROMOTION_PASS) {
            promotionPolicy = -PROMOTION_P;
        }

        double totalCost =
        		(BASE_COST + selectedDestination.getCost())
        		* numTravelers;
        totalCost = totalCost
        		- (totalCost * discount)
        		+ penaltyFee + promotionPolicy;

        System.out.println("Total cost of the vacation package: $" + totalCost);
    }
}
